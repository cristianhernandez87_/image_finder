import React, { Component } from 'react';
import Buscador from './components/Buscador';
import Resultado from './components/Resultado';

class App extends Component {

  state = {
    termino : '',
    imagenes : [],
    pagina : ''
  }
  
  scroll = () => {
    const elemento = document.querySelector('.jumbotron');
    elemento.scrollIntoView('smooth', 'start');
  }

  paginacionAnterior = () => {
    // leer state actual
    let pagina = this.state.pagina;

    // leer pagina 1, no regresa
    if(pagina === 1) return null;

    // sumar pagina
    pagina -=1;

    // agregar cambio state
    this.setState({
      pagina
    }, () => {
      this.consultarApi();
      this.scroll();
    });

    console.log(pagina);
  }

  paginacionSiguiente = () => {
    // leer state actual
    let pagina = this.state.pagina;

    // sumar pagina
    pagina ++;

    // agregar cambio state
    this.setState({
      pagina
    }, () => {
      this.consultarApi();
      this.scroll();
    });

    console.log(pagina);
  }

  consultarApi = () => {
    const termino = this.state.termino;
    const pagina = this.state.pagina;
    const url = `https://pixabay.com/api/?key=13054994-b32da11a024dd872e79d736c8&q=${termino}&per_page=40&page=${pagina}`;

    console.log(url);
    
    fetch(url)
      .then(respuesta => respuesta.json())
      .then(resultado => this.setState({ imagenes : resultado.hits}) )

  }

  datosBusqueda = (termino) => {
    this.setState({
      termino : termino,
      pagina : 1
    }, () => {
      this.consultarApi();
    })
  }

  render() {
    return (
      <div className="app container">
        <div className="jumbotron">
          <p className="lead text-center">Buscador de Imagenes</p>
          <Buscador
            datosBusqueda={this.datosBusqueda}
          />
        </div>
        <Resultado
          imagenes={this.state.imagenes}
          paginacionAnterior={this.paginacionAnterior}
          paginacionSiguiente={this.paginacionSiguiente}
        />
      </div>
    );
  }
}

export default App;
