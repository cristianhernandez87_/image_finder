import React from 'react';

const Paginacion = props => {
    return (
        <div className="py-3 text-center">
            <button onClick={props.paginacionAnterior} className="btn btn-info mr-1">&larr; Anterior</button>
            <button onClick={props.paginacionSiguiente} className="btn btn-info">Siguiente &rarr;</button>
        </div>
    )
}

export default Paginacion;